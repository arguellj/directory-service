# Distributed File Systems Task

## Summary

This is the third assignment for the module CS4400-Scalable Computing for the Master in Science of Computer Science at Trinity College Dublin.

This project specification of the project can be found at [Individual Programming Project](https://www.scss.tcd.ie/Stephen.Barrett/teaching/CS4400/individual_project.html).

## How do I get set up

### Application Dependencies

The project is built in Java 8. It uses [Maven](https://maven.apache.org/) as project management tool.
The following dependencies are included using Maven:

* [SpringBoot](https://projects.spring.io/spring-boot/) - Dependency Injection and RESTful Framework - [Maven Repository](https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-web)
* [JUnit](http://junit.org/junit4/) - Unit Testing Framework - [Maven Repository](https://mvnrepository.com/artifact/junit/junit)
* [Project Lombok](https://projectlombok.org/) - Library to simplify development - [Maven Repository](https://mvnrepository.com/artifact/org.projectlombok/lombok-maven)
* [Log4j2](https://logging.apache.org/log4j/2.x/) - Logging Utility - [Maven Repository](https://mvnrepository.com/artifact/log4j/log4j)
* [JsonPath](https://github.com/json-path/JsonPath) - Json Library - [Maven Repository](https://mvnrepository.com/artifact/com.jayway.jsonpath/json-path/2.4.0)

### Environment Dependencies

* [Maven](https://maven.apache.org/). If the build and execution are done locally. More information in section [Compile and run locally](#compile-and-run-locally).

* [Docker](https://www.docker.com). If the build and execution are done by using Docker. More information in section [Compile and run with Docker](#compile-and-run-with-docker).

### How to compile and run

The project can be compiled and run in two ways, locally or by using Docker. The following sections described these alternatives.

#### Compile and run locally

In order to run the project, it has to be compiled by using Maven with the following command:

> ./mvnw clean package

then, to execute it use the command:

> ./mvnw spring-boot:run

#### Compile and run with Docker

In order to run the project, it has to be compiled by using Maven with the following command:

> ./compile-with-docker

then, to execute it use the command:

> ./start-with-docker.sh

### Clarifications

#### Components Included

* **Security:** The security component follows the triple key protocol specified in the guideline. It uses AES as encryption mechanism with 128-bits for the keys. All the communications between client and server are produced securely by using secure requests and responses. The authentication server handles one key per server. The ticket has a expiration time which is set to 1 hour; after that the client has to authenticate again. Some parts of the design cannot be applied in real case scenarios:

    1. The keys must be generated and stored in a secure component. In this implementation the keys are stored in a DynamoDB Table. The generation of the key is done in the authentication server itself. A real case scenario must use Hardware Security Modules (HSMs). A suggestion is to use [Key Management Service](https://aws.amazon.com/kms/) from AWS.

    1. The password of the user is stored in a DynamoDB table. Besides that, it is stored as plain text. The password must be stored encrypted and hopefully in a secure service.

    1. The credentials of the user are verified as a simple comparison with the document record stored in the DynamoDb table mentioned above. The verification of identity must be managed through a service such as Kerberos.

* **Directory Server:** The directory service maps the file requested from the client to the current path in a file server. The assignation of the work is done randomly. This should be done by a Load Balancer, a service for real case is [Elastic Load Balancing](https://aws.amazon.com/elasticloadbalancing/) of AWS. In order to do this, the file servers must be behind a Virtual IP address that contains these servers. A service like [Virtual Private Cloud](https://aws.amazon.com/vpc/) would be useful along with ELB.

    This service also makes the callback to the clients to invalidate the cache when a client attempt to write to a file. Only the clients that have opened the same files are notified to clear their cache. However, this mechanism is not effective because is done before the write actually happens, therefore could happent that the clients clean their cache and the file did not change because something was wrong or simply the other client did not complete the write. It is important to notice that the cache of the client who is making the write is not invalidating, in fact its cache is updated adequately.

* **File Access Server:** The file server allows random access for read and write operations. It handles two parameters to allow this kind of access, a offset to specify from where start to read/write and a length to how many characters/bytes to read/write. When a write is done, it triggers a replication action to notify the others file server to overwrite the specified file with the new content sent by the file server which attended the write operation. All the write/read operations are shared exclusive lock. This includes writes and reads operation for replication purposes.

* **Cache:** The cache is stored in the client proxy. It contemplates the ranges of the current cache. So, only the parts of the file that are not cached are sent to the file server. So, if the current cache is from offset 20 and lenght 100, and a new read operation with offset 5 with length 50 is done; the client proxy gets from the cache the content from 20 to 35 and sent to read to the server the remaining (5 to 15). For write operations, the cache is updated after the operation is succesfully done. The invalidation of the cache is trigger by the directory service as mentioned above, because it is the only component which knows all the servers that have opened and read a file. Hence, it is the only component that can notify to all the clients that have read a specific file that the given file has changed.

* **Lock Service:** It implements a shared exclusive lock which allows concurrent access for read-only operations, while write operations require exclusive access. This means that multiple threads can read the data in parallel but an exclusive lock is needed for writing or modifying data. When a writer is writing the data, all other writers or readers will be blocked until the writer is finished writing.

* **Replication:** The replication mechanism used is Primary Server. All the write operations are redirecting to a single server. When a write occurrs, it triggers a replication action to the other file access servers asynchronously. The request contains the file name and the new content of the file specified. Then, the file access servers which get this replication request overwrite the file specified with the new content.

**NOTE:** Due to time, the template for the DynamoDB tables creation in [AWS Cloudformation](https://aws.amazon.com/cloudformation/) was not written, and therefore it was not commited as part of the repository. Although, it was not specified as part of the requirements of the project.

### Author Declaration

I have read and I understand the plagiarism provisions in the General Regulations of the University Calendar for the current year, found at: [http://www.tcd.ie/calendar](http://www.tcd.ie/calendar). I have also completed the Online Tutorial on avoiding plagiarism ‘Ready, Steady, Write’, located at [http://tcd-ie.libguides.com/plagiarism/ready-steady-write](http://tcd-ie.libguides.com/plagiarism/ready-steady-write) I declare that this assignment, together with any supporting artefact is offered for assessment as my original and unaided work, except in so far as any advice and/or assistance from any other named person in preparing it and any reference material used are duly and appropriately acknowledged.

### FAQ

**Q.** Which Maven version was used to build the project?

**A:** Maven 3.5.2

**Q.** How do I install Maven?

**A:** Please follow the steps mentioned in [Maven Installation Guide](https://maven.apache.org/install.html).

**Q.** I get error message: *Permission denied* when try to run any script.

**A:** Execute in the terminal console:

> chmod 755 start-with-docker.sh compile-with-docker.sh

**Q.** What does contain the Docker image?

**A:** It contains a lightweight linux distribution called alpine. The [docker image](https://hub.docker.com/_/maven/) used contains the JDK 8 and Maven 3.5.2, which are the dependencies of the applications. Then, Maven takes care of the specific dependencies of the application mentioned in the [Application Dependencies Section](#application-dependencies).

## Who do I talk to

* Jeancarlo Arguello Calvo - arguellj@tcd.ie
* StudentID: 17307291
* Trinity College Dublin, The University of Dublin