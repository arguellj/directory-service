package fileserver.dao;

import database.DynamoDatabase;
import fileserver.models.FileAccessServer;
import org.springframework.stereotype.Component;

@Component
public class FileAccessServerDAO extends DynamoDatabase<FileAccessServer> {
    @Override
    protected Class<FileAccessServer> getClassType() {
        return FileAccessServer.class;
    }
}
