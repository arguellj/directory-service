package fileserver;

import common.SecureCommunicator;
import common.models.ServerKey;
import common.models.requests.SecureRequest;
import common.models.Ticket;
import common.models.responses.SecureResponse;
import database.Database;
import fileserver.models.requests.ReadRequest;
import fileserver.models.requests.ReplicationRequest;
import fileserver.models.requests.WriteRequest;
import fileserver.services.FileServerService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
@Log4j2
public class FileServerController {

    @Value("${app.baseURL}")
    String baseUrl;

    @Autowired
    FileServerService fileServerService;

    @Autowired
    Database<ServerKey> serverKeyDAO;

    @RequestMapping(method = POST, value = "/read")
    public ResponseEntity<SecureResponse> read(
            @RequestBody @Valid SecureRequest request) {
        final Ticket ticket = SecureCommunicator.decryptTicket(request.getTicket(), this.baseUrl, this.serverKeyDAO);
        final ReadRequest readRequest = (ReadRequest) SecureCommunicator.decryptMessageAsObject(
                ticket.getSessionKey(), request.getMessage(), ReadRequest.class);
        log.info(String.format("Reading file: %s", readRequest.getFilePath()));

        final String data = this.fileServerService.read(readRequest.getFilePath(), readRequest.getOffset(), readRequest.getLength());

        return SecureCommunicator.getSecureResponse(ticket, data);
    }

    @RequestMapping(method = POST, value = "/write")
    public ResponseEntity<SecureResponse> write(
            @RequestBody @Valid SecureRequest request) {
        final Ticket ticket = SecureCommunicator.decryptTicket(request.getTicket(), this.baseUrl, this.serverKeyDAO);
        final WriteRequest writeRequest = (WriteRequest) SecureCommunicator.decryptMessageAsObject(
                ticket.getSessionKey(), request.getMessage(), WriteRequest.class);

        log.info(String.format("Writing file: %s", writeRequest.getFilePath()));
        this.fileServerService.write(writeRequest.getFilePath(), writeRequest.getOffset(), writeRequest.getData());
        return SecureCommunicator.getSecureResponse(ticket, "");
    }

    @RequestMapping(method = POST, value = "/replicate")
    public void replicate(@RequestBody @Valid ReplicationRequest request) {
        log.info(String.format("Received replication notification for file %s", request.getContent()));
        this.fileServerService.overwrite(request.getFilename(), request.getContent());
    }
}
