package fileserver.models.requests;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.nio.ByteBuffer;

@AllArgsConstructor
public class WriteRequest {

    @Getter
    @NotNull
    private String filePath;

    @NotNull
    @Getter
    private String data;

    @NotNull
    @Getter
    private int offset;
}
