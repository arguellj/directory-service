package fileserver.models.requests;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class ReplicationRequest {

    @Getter
    private String filename;

    @Getter
    private String content;
}
