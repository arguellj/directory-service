package fileserver.models;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import database.models.DynamoDocument;

@DynamoDBTable(tableName="FileAccessServer")
public class FileAccessServer implements DynamoDocument {

    private String name;

    @DynamoDBHashKey(attributeName="name")
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getPrimaryKey() {
        return this.name;
    }
}
