package fileserver.services;

public interface FileServerService {

    String read(String fileName, int offset, int length);

    void write(String fileName, int offset, String data);

    void overwrite(String fileName, String content);
}
