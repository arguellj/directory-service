package fileserver.services;

import fileserver.dao.FileAccessServerDAO;
import fileserver.models.FileAccessServer;
import fileserver.models.requests.ReplicationRequest;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Allows to aceess file in a random manner.
 */
@Log4j2
@Service
public class RandomAccessFileServer implements FileServerService {

    @Value("${app.filesystem.path}")
    private String folderPath;

    @Autowired
    private FileAccessServerDAO fileAccessServerDAO;

    @Value("${app.baseURL}")
    private String baseURL;

    /**
     * Map to handle the access to file in disk. When the operation is a write, only one thread can access the file.
     */
    private ConcurrentHashMap<String, ReentrantReadWriteLock> fileAccessLocker = new ConcurrentHashMap<>();

    /**
     * Reads the file in dish
     * @param fileName the filePath
     * @param offset the offset where start to read
     * @param length the amount of characters read
     * @return the content from offset to length
     */
    @Override
    public String read(String fileName, int offset, int length) {
        byte[] data = new byte[0];
        RandomAccessFile accessFile = null;

        this.fileAccessLocker.putIfAbsent(fileName, new ReentrantReadWriteLock());
        this.fileAccessLocker.get(fileName).readLock().lock();

        try {
            accessFile = new RandomAccessFile(this.folderPath + fileName, "r");
        } catch (FileNotFoundException e) {
            log.warn(e);
        }

        try {
            if (offset > accessFile.length() - 1) {
                offset = (int) accessFile.length() - 1;
            }

            if (accessFile.length() <= offset + length) {
                length = (int) Math.max(0, accessFile.length() - offset - 1);
            }

            data = new byte[length];
            accessFile.seek(offset);
            accessFile.read(data);
        } catch (IOException e) {
            log.error(e);
        } finally {

            this.fileAccessLocker.get(fileName).readLock().unlock();

            try {
                accessFile.close();
            } catch (IOException e) {
                log.warn(e);
            }
        }

        return new String(data, StandardCharsets.UTF_8);
    }

    /**
     * Writes content to a file in disk
     * @param fileName the filePath
     * @param offset the offset where start to write
     * @param data the data to write
     */
    @Override
    public void write(String fileName, int offset, String data) {
        RandomAccessFile accessFile = null;

        this.fileAccessLocker.putIfAbsent(fileName, new ReentrantReadWriteLock());
        this.fileAccessLocker.get(fileName).writeLock().lock();

        try {
            accessFile = new RandomAccessFile(this.folderPath + fileName, "rwd");
        } catch (FileNotFoundException e) {
            log.warn(e);
        }

        try {
            accessFile.seek(offset);
            accessFile.write(data.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            log.warn(e);
        } finally {
            CompletableFuture.runAsync(() -> this.replicateFile(fileName));
            this.fileAccessLocker.get(fileName).writeLock().unlock();

            try {
                accessFile.close();
            } catch (IOException e) {
                log.warn(e);
            }
        }
    }

    /**
     * Overwrite completely the file with content specified. Intended to be used for replication.
     * @param fileName the file to overwrite
     * @param content the content to write
     */
    public void overwrite(String fileName, String content) {
        log.info(String.format("Overwriting file %s because of replication", fileName));
        this.fileAccessLocker.get(fileName).writeLock().lock();
        try {
            try(Writer fileWriter = new FileWriter(this.folderPath + fileName, false)) {
                fileWriter.write(content);
            }
        } catch (IOException e) {
            log.error(e);
        }finally {
            this.fileAccessLocker.get(fileName).writeLock().unlock();
        }
    }

    private void replicateFile(String fileName) {
        log.info(String.format("Replicating file %s", fileName));
        String content = this.readCompletely(fileName);

        final List<FileAccessServer> allFileServers = this.fileAccessServerDAO.getAll();
        allFileServers.parallelStream()
                .filter(fileAccessServer -> !fileAccessServer.getName().equals(this.baseURL))
                .forEach(fileAccessServer -> {
                    log.info(String.format("Notifying replication to %s", fileAccessServer.getName()));
            RestTemplate template = new RestTemplate();
            template.postForLocation(fileAccessServer.getName(), new ReplicationRequest(fileName, content));
        });
    }

    private String readCompletely(String fileName) {
        try {
            log.debug(String.format("Reading completely the file %s", fileName));
            return new String(Files.readAllBytes(Paths.get(this.folderPath + fileName)), StandardCharsets.UTF_8);
        } catch (IOException e) {
            log.error(e);
        }

        return null;
    }
}
