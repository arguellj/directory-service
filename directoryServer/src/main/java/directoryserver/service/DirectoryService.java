package directoryserver.service;

/**
 * The directory service is responsible for mapping human readable, global file names into file identifiers used by
 * the file system itself.
 * A user request to open a particular file X should be passed by the client proxy to the directory server for resolution.
 */
public interface DirectoryService {
    /**
     * Maps global file names into file identifiers used by the file system itself
     * @param filePath global file name
     * @param isWrite whether or not the operation is for writing
     * @param client the client url who is requesting the operation
     * @return identifier to the file system
     */
    String mapFile(String filePath, boolean isWrite, String client);
}
