package directoryserver.service;

import database.Database;
import directoryserver.exceptions.FileNotFoundException;
import directoryserver.models.Directory;
import common.models.ServerKey;
import common.models.Ticket;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import security.AESEncryption;

import java.io.File;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

@Service
@Log4j2
public class DatabaseDirectoryService implements DirectoryService {

    @Autowired
    private Database<Directory> directoryDAO;

    @Value("${primaryFileServer}")
    private String primaryServer;

    /**
     * Map for track the files opened by user to make callback when a write happen.
     */
    private ConcurrentHashMap<String, Set<String>> clientsConnected = new ConcurrentHashMap();

    @Override
    public String mapFile(final String filePath, final boolean isWrite, String client) {
        final File fileToMap = new File(filePath);
        final String directoryKey = fileToMap.getParent();
        final String fileName = fileToMap.getName();

        if (directoryKey == null) {
            String error = "Not specified any directory in the path of the file.";
            log.warn(error);
            throw new FileNotFoundException(error);
        }

        if (fileName == null) {
            String error = "Not specified file in the path.";
            log.warn(error);
            throw new FileNotFoundException(error);
        }

        final Directory directory = this.directoryDAO.get(directoryKey);

        if (directory == null) {
            String error = String.format("Directory %s does not exist", directoryKey);
            log.warn(error);
            throw new FileNotFoundException(error);
        }

        if (!directory.getFiles().contains(fileName)) {
            String error = String.format("File %s does not exist", fileName);
            log.warn(error);
            throw new FileNotFoundException(error);
        }

        if (isWrite) {
            CompletableFuture.runAsync(() -> this.sendInvalidateCache(filePath, client));
        }

        this.clientsConnected.putIfAbsent(filePath, new HashSet<>());
        this.clientsConnected.get(filePath).add(client);

        return directory.assignFileServer() + filePath;
    }

    private void sendInvalidateCache(String filePath, String clientWriting) {
        log.info(String.format("Sending invalidation cache callback for file %s", filePath));
        this.clientsConnected.get(filePath).parallelStream()
                .filter(clientURL -> !clientURL.equals(clientWriting))
                .forEach(clientURL -> {
                    log.info(String.format("Callback to %s", clientURL));
                    RestTemplate restTemplate = new RestTemplate();
                    restTemplate.postForLocation(clientURL, filePath);
                });
    }
}
