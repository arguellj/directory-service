package directoryserver.models;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import database.annotations.TableName;
import database.models.DynamoDocument;
import lombok.Getter;

import java.util.List;
import java.util.Random;
import java.util.Set;

@DynamoDBTable(tableName="Directory")
public class Directory implements DynamoDocument {

    private String path;

    private List<String> files;

    private List<String> fileServers;

    public Directory() { }

    public Directory(String path, List<String> files, List<String> fileServers) {
        this.path = path;
        this.files = files;
        this.fileServers = fileServers;
    }

    @DynamoDBHashKey(attributeName="path")
    public String getPath() {
        return path;
    }
    public void setPath(String path) {
        this.path = path;
    }

    @DynamoDBAttribute(attributeName = "files")
    public List<String> getFiles() {
        return files;
    }
    public void setFiles(List<String> files) {
        this.files = files;
    }

    @DynamoDBAttribute(attributeName = "fileServers")
    public List<String> getFileServer() {
        return fileServers;
    }
    public void setFileServer(List<String> fileServers) {
        this.fileServers = fileServers;
    }

    @Override
    public String getPrimaryKey() {
        return this.path;
    }

    @DynamoDBIgnore
    public String assignFileServer() {
        Random rand = new Random();
        String fileServer = this.fileServers.get(rand.nextInt(this.fileServers.size()));
        return fileServer;
    }
}
