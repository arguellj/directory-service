package directoryserver.models.requests;

import lombok.Getter;

public class InvalidCacheRequest {

    @Getter
    private String filePath;
}
