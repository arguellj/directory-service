package directoryserver.models.requests;

import lombok.Getter;

import javax.validation.constraints.NotNull;

public class FileRequest {

    @Getter
    @NotNull
    private String filePath;

    @Getter
    private boolean writeOperation;
}
