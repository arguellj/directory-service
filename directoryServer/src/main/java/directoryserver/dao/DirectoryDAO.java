package directoryserver.dao;

import database.DynamoDatabase;
import directoryserver.models.Directory;
import org.springframework.stereotype.Component;

@Component
public class DirectoryDAO extends DynamoDatabase<Directory> {
    @Override
    protected Class<Directory> getClassType() {
        return Directory.class;
    }
}
