package directoryserver;

import common.SecureCommunicator;
import common.models.ServerKey;
import common.models.requests.SecureRequest;
import common.models.responses.SecureResponse;
import common.models.Ticket;
import database.Database;
import directoryserver.models.requests.FileRequest;
import directoryserver.service.DirectoryService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@Log4j2
public class DirectoryController {

    @Value("${app.baseURL}")
    String baseUrl;

    @Autowired
    DirectoryService directoryService;

    @Autowired
    Database<ServerKey> serverKeyDAO;

    @RequestMapping(method = POST, value = "/map")
    public ResponseEntity<SecureResponse> getLocation(@RequestBody @Valid SecureRequest request,
                                                      HttpServletRequest httpRequest) {
        String host  = httpRequest.getRemoteHost();
        String scheme = httpRequest.getScheme();
        int port = httpRequest.getRemotePort();

        String clientURL = scheme + "://" + host + ":" + port;

        log.debug("Mapping file...");

        final Ticket ticket = SecureCommunicator.decryptTicket(request.getTicket(), this.baseUrl, this.serverKeyDAO);
        final FileRequest fileRequest = (FileRequest) SecureCommunicator
                .decryptMessageAsObject(ticket.getSessionKey(), request.getMessage(), FileRequest.class);

        final String location = this.directoryService.mapFile(
                fileRequest.getFilePath(),fileRequest.isWriteOperation(), clientURL);
        log.info(String.format("File %s map to %s for %s", fileRequest.getFilePath(), location, clientURL));
        return SecureCommunicator.getSecureResponse(ticket, location);
    }

}
