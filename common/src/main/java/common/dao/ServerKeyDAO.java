package common.dao;

import common.models.ServerKey;
import database.DynamoDatabase;
import org.springframework.stereotype.Component;

@Component
public class ServerKeyDAO extends DynamoDatabase<ServerKey> {
    @Override
    protected Class<ServerKey> getClassType() {
        return ServerKey.class;
    }
}
