package common;

import common.models.ServerKey;
import common.models.Ticket;
import common.models.responses.SecureResponse;
import database.Database;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import security.AESEncryption;

/**
 * Library to deal with the security communication protocol
 */
public class SecureCommunicator {

    /**
     * Creates a secure response that can be used to communicate with a client
     * @param ticket the ticket to sent
     * @param message the message to encrypt
     * @return a secure response
     */
    public static ResponseEntity<SecureResponse> getSecureResponse(Ticket ticket, String message) {
        final String encrypt = AESEncryption.encrypt(Base64.decodeBase64(ticket.getSessionKey()), message);
        final SecureResponse response = new SecureResponse(encrypt);
        return ResponseEntity.ok(response);
    }

    /**
     * Creates a secure response that can be used to communicate with a client
     * @param ticket the ticket to sent
     * @param object the object to encrypt
     * @return a secure response
     */
    public static ResponseEntity<SecureResponse> getSecureResponse(Ticket ticket, Object object) {
        final String encrypt = AESEncryption.encrypt(Base64.decodeBase64(ticket.getSessionKey()), object);
        final SecureResponse response = new SecureResponse(encrypt);
        return ResponseEntity.ok(response);
    }

    /**
     * Decrypts a message from a client
     * @param sessionKey the key shared with a client
     * @param messageEncrypted the message to decrypt
     * @return the message decrypted
     */
    public static String decryptMessageAsString(String sessionKey, String messageEncrypted) {
        String message = AESEncryption.decryptAsString(Base64.decodeBase64(sessionKey), messageEncrypted);
        return message;
    }

    /**
     * Decrypts a message from a client
     * @param sessionKey the key shared with a client
     * @param messageEncrypted the message to decrypt
     * @param objectClass class of the object to return
     * @return the message decrypted
     */
    public static Object decryptMessageAsObject(String sessionKey, String messageEncrypted, Class<?> objectClass) {
        return AESEncryption.decryptAsObject(Base64.decodeBase64(sessionKey), messageEncrypted, objectClass);
    }

    /**
     * Decrypts a ticket
     * @param ticketEncrypted the ticket encypted
     * @param serverUrl the server who has the key
     * @param serverKeyDAO the identifier of the server
     * @return the Ticket decrypted
     */
    public static Ticket decryptTicket(String ticketEncrypted, String serverUrl, Database<ServerKey> serverKeyDAO) {
        final ServerKey serverKey = serverKeyDAO.get(serverUrl);
        final Ticket ticket = (Ticket) AESEncryption.decryptAsObject(serverKey.getKey(), ticketEncrypted, Ticket.class);
        return ticket;
    }
}
