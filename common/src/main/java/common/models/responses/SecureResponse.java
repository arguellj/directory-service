package common.models.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class SecureResponse {

    @Getter
    private String message;
}
