package common.models.requests;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
public class SecureRequest {

    @Getter
    @NotNull
    private String message;

    @Getter
    @NotNull
    private String ticket;
}
