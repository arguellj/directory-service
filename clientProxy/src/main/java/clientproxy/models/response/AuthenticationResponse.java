package clientproxy.models.response;

import lombok.Getter;

public class AuthenticationResponse {

    /**
     * Token encrypted with a key derived from the user's password passed to the AS.
     */
    @Getter
    private String token;
}
