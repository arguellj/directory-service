package clientproxy.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;

@AllArgsConstructor
public class Token {

    @Getter
    private String sessionKey;

    @Getter
    private String ticket;

    @Getter
    private String serverIdentity;

    @Getter
    private Date expiration;
}
