package clientproxy.models.requests;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
public class FileRequest {

    @Getter
    @NotNull
    private String filePath;

    @Getter
    private boolean writeOperation;
}
