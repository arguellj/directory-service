package clientproxy.models.requests;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class LockRequest {

    @Getter
    private String filePath;

    @Getter
    private boolean writeOperation;
}
