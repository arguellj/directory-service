package clientproxy.models.requests;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class LoginRequest {

    @Getter
    private String message;

    @Getter
    private String username;

    @Getter
    private String server;
}
