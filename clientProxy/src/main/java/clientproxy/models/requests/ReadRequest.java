package clientproxy.models.requests;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
public class ReadRequest {

    @NotNull
    @Getter
    private String filePath;

    @NotNull
    @Getter
    private int offset;

    @NotNull
    @Getter
    private int length;
}
