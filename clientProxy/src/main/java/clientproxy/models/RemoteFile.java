package clientproxy.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class RemoteFile {

    @Getter
    @Setter
    private String handler;

    @Getter
    @Setter
    private StringBuilder cacheContent;

    @Getter
    @Setter
    private int lowerOffset;
}
