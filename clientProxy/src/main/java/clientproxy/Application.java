package clientproxy;

import clientproxy.api.ClientAPI;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        final ConfigurableApplicationContext context = SpringApplication.run(clientproxy.Application.class, args);
        final ClientAPI api = context.getBean(ClientAPI.class);

        String file = "/test/notes.txt";
        api.open(file);
        String content = api.read(file, 50, 200);
        System.out.println("No." + content.length());
        System.out.println(content);

        content = api.read(file, 50, 50);
        System.out.println("No." + content.length());
        System.out.println(content);

        String newMsg = "   WRITING ONE LINE 3213NO2JEOD CODSCASOCSAC ADSOCJ ASOCJAOSJCOAS COIAS...";
        api.write(file, 25, newMsg);
        newMsg = "In the beiignning ";

        api.write(file, 1, newMsg);
        content = api.read(file, 0, 700);
        System.out.println("No." + content.length());
        System.out.println(content);
    }
}