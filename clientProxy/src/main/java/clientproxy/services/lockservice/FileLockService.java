package clientproxy.services.lockservice;

import clientproxy.models.requests.LockRequest;
import clientproxy.services.CommonServices;
import clientproxy.services.ServerService;
import common.models.requests.SecureRequest;
import common.models.responses.SecureResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class FileLockService
        extends ServerService
        implements LockService {

    @Value("${app.lockserver.baseUrl}")
    private String baseUrl;

    @Value("${app.username}")
    private String username;

    @Value("${app.password}")
    private String password;

    @Override
    public void lock(String filePath, boolean isWrite) {
        this.authenticate(this.username, this.password);
        RestTemplate restTemplate = new RestTemplate();
        LockRequest lockRequest = new LockRequest(filePath, isWrite);
        final SecureRequest request = CommonServices.getSecureRequest(this.token, lockRequest);
        restTemplate.postForObject( this.baseUrl + "/lock", request, SecureResponse.class);
    }

    @Override
    public void release(String filePath, boolean isWrite) {
        this.authenticate(this.username, this.password);
        RestTemplate restTemplate = new RestTemplate();
        LockRequest lockRequest = new LockRequest(filePath, isWrite);
        final SecureRequest request = CommonServices.getSecureRequest(this.token, lockRequest);
        restTemplate.postForObject( this.baseUrl + "/release", request, SecureResponse.class);
    }

    @Override
    protected String getServerName() {
        return this.baseUrl;
    }
}
