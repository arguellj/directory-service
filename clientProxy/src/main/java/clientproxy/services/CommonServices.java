package clientproxy.services;

import clientproxy.models.Token;
import common.models.requests.SecureRequest;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import security.AESEncryption;

@Component
public class CommonServices {

    public static SecureRequest getSecureRequest(Token token, String message) {
        final String encrypt = AESEncryption.encrypt(Base64.decodeBase64(token.getSessionKey()), message);
        final SecureRequest response = new SecureRequest(encrypt, token.getTicket());
        return response;
    }

    public static SecureRequest getSecureRequest(Token token, Object object) {
        final String encrypt = AESEncryption.encrypt(Base64.decodeBase64(token.getSessionKey()), object);
        final SecureRequest response = new SecureRequest(encrypt, token.getTicket());
        return response;
    }

    public static String getSecureResponseMessage(String key, String message) {
        return AESEncryption.decryptAsString(Base64.decodeBase64(key), message);
    }

    public static Object getSecureResponseMessage(String key, String message, Class<?> objectClass) {
        return AESEncryption.decryptAsObject(Base64.decodeBase64(key), message, objectClass);
    }
}
