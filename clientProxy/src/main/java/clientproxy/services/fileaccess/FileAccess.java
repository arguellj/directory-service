package clientproxy.services.fileaccess;

import clientproxy.services.autenticationservice.Authentication;

import java.net.URISyntaxException;
import java.nio.ByteBuffer;

public interface FileAccess extends Authentication{

    void open(String fileHandler);

    String read(String fileHandler, int offset, int length) throws URISyntaxException;

    void write(String fileHandler, int offset, String data) throws URISyntaxException;

    void close(String fileHandler);
}
