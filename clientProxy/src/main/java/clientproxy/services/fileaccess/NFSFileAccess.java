package clientproxy.services.fileaccess;

import clientproxy.models.requests.ReadRequest;
import clientproxy.models.requests.WriteRequest;
import clientproxy.services.CommonServices;
import clientproxy.services.ServerService;
import clientproxy.services.lockservice.LockService;
import common.models.requests.SecureRequest;
import common.models.responses.SecureResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;

@Component
@Log4j2
public class NFSFileAccess
        extends ServerService
        implements FileAccess {

    @Autowired
    private LockService lockService;

    private String baseUrl;

    @Value("${app.username}")
    private String username;

    @Value("${app.password}")
    private String password;

    @Override
    public void open(String fileHandler) {
        return;
    }

    @Override
    public String read(String fileHandler, int offset, int length) throws URISyntaxException {
        URI uri = new URI(fileHandler);
        this.lockService.lock(uri.getPath(), false);
        this.baseUrl = this.getServerBaseUrl(uri);
        this.authenticate(this.username, this.password);
        RestTemplate restTemplate = new RestTemplate();
        ReadRequest readRequest = new ReadRequest(uri.getPath(), offset, length);
        final SecureRequest request = CommonServices.getSecureRequest(this.token, readRequest);
        final SecureResponse response = restTemplate.postForObject( this.baseUrl + "/read", request, SecureResponse.class);
        this.lockService.release(uri.getPath(), false);
        String data = CommonServices.getSecureResponseMessage(
                this.token.getSessionKey(), response.getMessage());
        return data;
    }

    @Override
    public void write(String fileHandler, int offset, String data) throws URISyntaxException {
        URI uri = new URI(fileHandler);
        this.lockService.lock(uri.getPath(), true);
        this.baseUrl = this.getServerBaseUrl(uri);
        this.authenticate(this.username, this.password);
        RestTemplate restTemplate = new RestTemplate();
        WriteRequest writeRequest = new WriteRequest(uri.getPath(), data, offset);
        final SecureRequest request = CommonServices.getSecureRequest(this.token, writeRequest);
        restTemplate.postForObject(this.baseUrl + "/write", request, SecureResponse.class);
        this.lockService.release(uri.getPath(), true);
    }

    @Override
    public void close(String fileHandler) {
        return;
    }

    @Override
    protected String getServerName() {
        return this.baseUrl;
    }

    private String getServerBaseUrl(URI uri) {
        return uri.getScheme() + "://" + uri.getHost() + ":" + uri.getPort();
    }
}
