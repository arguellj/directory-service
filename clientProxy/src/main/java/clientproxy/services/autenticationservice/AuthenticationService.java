package clientproxy.services.autenticationservice;

import clientproxy.models.Token;

public interface AuthenticationService {

    Token authenticate(String username, String password, String serverToConnect);

    boolean isValid(Token token);
}
