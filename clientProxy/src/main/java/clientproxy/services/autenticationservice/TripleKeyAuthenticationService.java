package clientproxy.services.autenticationservice;

import clientproxy.models.Token;
import clientproxy.models.requests.LoginRequest;
import clientproxy.models.response.AuthenticationResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import security.AESEncryption;

import java.util.Calendar;
import java.util.Date;

@Log4j2
@Service
public class TripleKeyAuthenticationService implements AuthenticationService {

    @Value("${app.authenticationserver.baseUrl}")
    String baseUrl;

    @Override
    public Token authenticate(final String username, final String password, final String serverToConnect) {
        final String encrypt = AESEncryption.encrypt(password, password);
        final LoginRequest request = new LoginRequest(encrypt, username, serverToConnect);
        RestTemplate restTemplate = new RestTemplate();
        final AuthenticationResponse response = restTemplate.postForObject(baseUrl + "/authenticate", request, AuthenticationResponse.class);
        Token token = (Token) AESEncryption.decryptAsObject(password, response.getToken(), Token.class);
        return token;
    }

    @Override
    public boolean isValid(Token token) {
        return token != null && this.getValidTime().before(token.getExpiration());
    }

    private Date getValidTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.MINUTE, 1);
        return calendar.getTime();
    }
}
