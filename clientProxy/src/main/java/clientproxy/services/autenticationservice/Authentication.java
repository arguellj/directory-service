package clientproxy.services.autenticationservice;

public interface Authentication {

    void authenticate(String username, String password);
}
