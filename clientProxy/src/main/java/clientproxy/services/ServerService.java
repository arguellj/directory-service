package clientproxy.services;

import clientproxy.models.Token;
import clientproxy.services.autenticationservice.Authentication;
import clientproxy.services.autenticationservice.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class ServerService implements Authentication {

    @Autowired
    private AuthenticationService authenticationService;

    protected Token token;

    @Override
    public void authenticate(String username, String password) {
        if (!this.authenticationService.isValid(this.token)) {
            this.token = this.authenticationService.authenticate(username, password, this.getServerName());
        }
    }

    protected abstract String getServerName();

}
