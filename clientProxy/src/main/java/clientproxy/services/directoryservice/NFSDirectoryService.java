package clientproxy.services.directoryservice;

import clientproxy.models.requests.FileRequest;
import clientproxy.services.CommonServices;
import clientproxy.services.ServerService;
import common.models.requests.SecureRequest;
import common.models.responses.SecureResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@Log4j2
public class NFSDirectoryService
        extends ServerService
        implements DirectoryService {

    @Value("${app.directoryserver.baseUrl}")
    private String baseUrl;

    @Value("${app.username}")
    private String username;

    @Value("${app.password}")
    private String password;

    @Override
    public String getLocation(final String filename, boolean isWrite) {
        this.authenticate(this.username, this.password);
        RestTemplate restTemplate = new RestTemplate();
        final SecureRequest request = CommonServices.getSecureRequest(this.token, new FileRequest(filename, isWrite));
        final SecureResponse response = restTemplate.postForObject(this.baseUrl + "/map",  request, SecureResponse.class);
        String location = CommonServices.getSecureResponseMessage(this.token.getSessionKey(), response.getMessage());
        return location;
    }

    @Override
    protected String getServerName() {
        return this.baseUrl;
    }
}
