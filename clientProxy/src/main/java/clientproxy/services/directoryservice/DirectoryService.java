package clientproxy.services.directoryservice;

public interface DirectoryService {

    /**
     * Gets location of file access server that has the file with name {@param filename}.
     * @param filename name of the file
     * @return path of the file server which contains the file
     */
    String getLocation(final String filename, boolean isWrite);
}
