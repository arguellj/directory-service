package clientproxy.api;

import clientproxy.exceptions.FileAlreadyOpenedException;
import clientproxy.exceptions.NotOpenedFileException;
import clientproxy.models.RemoteFile;
import clientproxy.services.directoryservice.DirectoryService;
import clientproxy.services.fileaccess.FileAccess;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.constraints.NotNull;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
@Log4j2
public class OrchestratorClientAPI implements ClientAPI {

    @Autowired
    private FileAccess fileAccessService;

    @Autowired
    private DirectoryService directoryService;

    private HashMap<String, RemoteFile> filesOpened = new HashMap<>();

    private Lock lock = new ReentrantLock(true);

    @Override
    public void open(String filename) {
        if (this.filesOpened.containsKey(filename)) {
            throw new FileAlreadyOpenedException(String.format("The file %s has already been opened.", filename));
        }

        String fileHandler = this.directoryService.getLocation(filename, false);
        this.filesOpened.putIfAbsent(filename, new RemoteFile(fileHandler, new StringBuilder(), Integer.MAX_VALUE));
    }

    @Override
    public String read(String filename, int offset, int length) {
        this.validateCall(filename);

        if (length == 0) {
            return "";
        }

        StringBuilder cache = new StringBuilder();
        this.lock.lock();
        if (this.filesOpened.get(filename).getCacheContent().length() > 0) {
            RemoteFile file = this.filesOpened.get(filename);
            int cacheLength, cacheOffset = 0;

            if (file.getLowerOffset() <= offset) {
                cacheOffset = offset - file.getLowerOffset();
                cacheLength = Math.min(cacheOffset + length, file.getCacheContent().length());

                // A part of the read was cache. Update values by reducing properly.
                offset += Math.abs(cacheLength - cacheOffset);
                length -= Math.abs(cacheLength - cacheOffset);
            } else {
                // If the requested offset is lower and the requested length is higher, the cache has information in the
                // middle. So, if proceeds it will create 2 slices to read, and it would mean send to different request
                // to the server; one for the lower slice and other for the upper slice created bt caching something in
                // the middle. In this case, it is better to send a request to read the entire piece because probably it
                // cheaper to read the file rather than send and receive two requests.
                if (length > file.getCacheContent().length()) {
                    cacheLength = 0;
                } else {
                    cacheLength = Math.min(
                            Math.max(0, length - Math.abs(file.getLowerOffset() - offset)),
                            file.getCacheContent().length());

                    if (cacheLength > 0) {
                        // A part of the read was cache. Update values by reducing properly.
                        length = file.getLowerOffset() - offset;
                    }
                }
            }

            cache.append(this.filesOpened.get(filename).getCacheContent().substring(cacheOffset, cacheLength));

            // Nothing to read. The entire read was cached.
            if (length == 0) {
                return cache.toString();
            }
        }

        try {
            String fileHandler = this.directoryService.getLocation(filename, true);
            final String read = this.fileAccessService.read(fileHandler, offset, length);

            // Update the cache.
            RemoteFile file = this.filesOpened.get(filename);

            // Delete cache if new portion is not adjacent (complex scenario to handle)
            // TODO: Manage not adjacent read portions in the cache.
            if (file.getLowerOffset() == Integer.MAX_VALUE ||
                    (offset + read.length() < file.getLowerOffset()
                            || (offset > file.getLowerOffset() + file.getCacheContent().length()))) {
                file.setCacheContent(new StringBuilder(read));
                file.setLowerOffset(offset);
                cache.insert(0, read);
            } else if (offset < file.getLowerOffset()) {
                // Read portion is before that cache portion.
                this.filesOpened.get(filename).getCacheContent().insert(offset, read);
                this.filesOpened.get(filename).setLowerOffset(offset);
                cache.insert(0, read);
            } else {
                // Read portion is after that cache portion.
                this.filesOpened.get(filename).getCacheContent().append(read);
                cache.append(read);
            }

            return cache.toString();
        } catch (URISyntaxException e) {
            log.error(e);
        } finally {
            this.lock.unlock();
        }

        return null;
    }

    @Override
    public void write(String filename, int offset, String data) {
        this.validateCall(filename);

        // Nothing to write
        if (data.length() == 0) {
            return;
        }

        try {
            this.fileAccessService.write(this.filesOpened.get(filename).getHandler(), offset, data);
        } catch (URISyntaxException e) {
            log.error(e);
        }

        this.lock.lock();
        // Update cache
        if (this.filesOpened.get(filename).getCacheContent().length() > 0) {
            RemoteFile file = this.filesOpened.get(filename);
            int cacheLength, cacheOffset = 0;
            String dataToReplace;

            if (file.getLowerOffset() <= offset) {
                cacheOffset = offset - file.getLowerOffset();
                cacheLength = Math.min(cacheOffset + data.length(), file.getCacheContent().length());
                dataToReplace = data.substring(0, data.length());
                this.filesOpened.get(filename).getCacheContent().replace(cacheOffset, cacheLength, dataToReplace);
            } else {
                int difOffsets = file.getLowerOffset() - offset;
                cacheLength = Math.min(
                        Math.max(0, data.length() - difOffsets),
                        file.getCacheContent().length());

                if (cacheLength > 0) {
                    dataToReplace = data.substring(difOffsets, data.length());
                    this.filesOpened.get(filename).getCacheContent().replace(cacheOffset, cacheLength, dataToReplace);
                    this.filesOpened.get(filename).getCacheContent()
                            .insert(0, data.substring(0, difOffsets));
                    this.filesOpened.get(filename).setLowerOffset(offset);
                }
            }
        }

        this.lock.unlock();
    }

    @Override
    public void close(String filename) {
        this.validateCall(filename);
        this.fileAccessService.close(this.filesOpened.get(filename).getHandler());
        this.filesOpened.remove(filename);
    }

    @RequestMapping(method = POST)
    public void invalidateCache(@RequestBody @NotNull String filename) {
        log.info(String.format("Cleaning cache for %s", filename));
        this.lock.lock();
        if (this.filesOpened.containsKey(filename)) {
            this.filesOpened.get(filename).setCacheContent(new StringBuilder());
        }
        this.lock.unlock();
    }

    private void validateCall(String filename) {
        if (!this.filesOpened.containsKey(filename)) {
            throw new NotOpenedFileException(String.format("The file %s has not been opened.", filename));
        }
    }
}
