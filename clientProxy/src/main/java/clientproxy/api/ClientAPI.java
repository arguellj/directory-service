package clientproxy.api;

public interface ClientAPI {

    void open(String filename);

    String read(String filename, int offset, int bytesToRead);

    void write(String filename, int offset, String data);

    void close(String filename);
}
