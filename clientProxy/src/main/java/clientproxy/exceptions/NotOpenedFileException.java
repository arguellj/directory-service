package clientproxy.exceptions;

public class NotOpenedFileException extends RuntimeException {
    public NotOpenedFileException(String message) {
        super(message);
    }

    public NotOpenedFileException(String message, Throwable cause) {
        super(message, cause);
    }
}
