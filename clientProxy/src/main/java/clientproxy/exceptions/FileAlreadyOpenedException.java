package clientproxy.exceptions;

public class FileAlreadyOpenedException extends RuntimeException {
    public FileAlreadyOpenedException(String message) {
        super(message);
    }

    public FileAlreadyOpenedException(String message, Throwable cause) {
        super(message, cause);
    }
}
