package security;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.codec.binary.Base64;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * Library for AES Encryption with 128 bits
 */
@Log4j2
public class AESEncryption {

    /**
     * TODO: This variables should be stored and generated in and from a safety place.
     */
    private static final String INIT_VECTOR = "RandomInitVector"; // 16 bytes IV

    /**
     * Encrypts a object with the key provided
     * @param key the key to encrypt with
     * @param object the object to encrypt
     * @return the object encrypted
     */
    public static String encrypt(String key, Object object) {
        try {
            return encrypt(key.getBytes("UTF-8"), object);
        } catch (UnsupportedEncodingException e) {
            log.error(e);
        }

        return null;
    }

    /**
     * Encrypts a object with the key provided
     * @param key the key to encrypt with
     * @param object the object to encrypt
     * @return the object encrypted
     */
    public static String encrypt(byte[] key, Object object) {
        ObjectMapper objectMapper = new ObjectMapper();

        String json = null;
        try {
            json = objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            log.warn(e);
        }

        final String encrypt = AESEncryption.encrypt(key, json);

        return encrypt;
    }

    /**
     * Encrypts a object with the key provided
     * @param key the key to encrypt with
     * @param value the object to encrypt
     * @return the object encrypted
     */
    public static String encrypt(String key, String value)  {
        try {
            return encrypt(key.getBytes("UTF-8"), value);
        } catch (UnsupportedEncodingException e) {
            log.error(e);
        }

        return null;
    }

    /**
     * Encrypts a object with the key provided
     * @param key the key to encrypt with
     * @param value the object to encrypt
     * @return the object encrypted
     */
    public static String encrypt(byte[] key, String value) {
        try {
            IvParameterSpec iv = new IvParameterSpec(INIT_VECTOR.getBytes("UTF-8"));

            SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

            byte[] encrypted = cipher.doFinal(value.getBytes());

            return Base64.encodeBase64String(encrypted);
        } catch (Exception e) {
            log.error(e);
        }

        return null;
    }

    /**
     * Decrypt a message into a object of the class specified
     * @param key the key to decrypt with
     * @param encrypted the encrypte message to decrypt
     * @param objectClass the class of the object to return
     * @return a object of the class provided
     */
    public static Object decryptAsObject(String key, String encrypted, Class<?> objectClass) {
        try {
            return decryptAsObject(key.getBytes("UTF-8"), encrypted, objectClass);
        } catch (UnsupportedEncodingException e) {
            log.error(e);
            return null;
        }
    }

    /**
     * Decrypt a message into a object of the class specified
     * @param key the key to decrypt with
     * @param encrypted the encrypte message to decrypt
     * @param objectClass the class of the object to return
     * @return a object of the class provided
     */
    public static Object decryptAsObject(byte[] key, String encrypted, Class<?> objectClass) {
        final String decrypt = AESEncryption.decryptAsString(key, encrypted);
        ObjectMapper objectMapper = new ObjectMapper();

        Object object = null;

        try {
            object = objectMapper.readValue(decrypt, objectClass);
        } catch (IOException e) {
            log.warn(e);
        }

        return object;
    }

    /**
     * Decrypt a message into a object of the class specified
     * @param key the key to decrypt with
     * @param encrypted the encrypte message to decrypt
     * @return the message decrypted
     */
    public static String decryptAsString(String key, String encrypted) {
        try {
            return decryptAsString(key.getBytes("UTF-8"), encrypted);
        } catch (UnsupportedEncodingException e) {
            log.error(e);
        }

        return null;
    }

    /**
     * Decrypt a message into a object of the class specified
     * @param key the key to decrypt with
     * @param encrypted the encrypte message to decrypt
     * @return the message decrypted
     */
    public static String decryptAsString(byte[] key, String encrypted) {
        try {
            IvParameterSpec iv = new IvParameterSpec(INIT_VECTOR.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

            byte[] original = cipher.doFinal(Base64.decodeBase64(encrypted));

            return new String(original);
        } catch (Exception e) {
            log.error(e);
        }

        return null;
    }

    /**
     * Generates a random secure key
     * @return the key
     */
    public static String generateKey() {
        KeyGenerator keyGen = null;

        try {
            keyGen = KeyGenerator.getInstance("AES");
        } catch (NoSuchAlgorithmException e) {
            log.error(e);
        }

        SecureRandom rand = new SecureRandom();
        keyGen.init(128, rand);
        SecretKey secretKey = keyGen.generateKey();
        return Base64.encodeBase64String(secretKey.getEncoded());
    }
}