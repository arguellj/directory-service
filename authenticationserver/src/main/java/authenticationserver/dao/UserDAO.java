package authenticationserver.dao;

import authenticationserver.models.User;
import database.DynamoDatabase;
import org.springframework.stereotype.Component;

@Component
public class UserDAO extends DynamoDatabase<User> {

    @Override
    protected Class<User> getClassType() {
        return User.class;
    }
}
