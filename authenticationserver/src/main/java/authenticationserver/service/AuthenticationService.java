package authenticationserver.service;

public interface AuthenticationService {
    /**
     * Verify if the credentials are correct.
     * @param username the username of the user to verify
     * @param encryptPassword the password encrypted of the user to verify
     * @return the password decrypt
     */
    String verifyUser(final String username, final String encryptPassword);

    /**
     * Creates the token to send to the client authenticating
     * @param server the server that the client was to connect to
     * @param password the password of the client
     * @return the token encrypted by the password
     */
    String createToken(final String server, final String password);
}
