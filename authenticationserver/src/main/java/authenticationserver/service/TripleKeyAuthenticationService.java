package authenticationserver.service;

import authenticationserver.exceptions.UnauthorizedException;
import authenticationserver.models.ServerKey;
import authenticationserver.models.Ticket;
import authenticationserver.models.Token;
import authenticationserver.models.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import database.Database;
import database.DynamoDatabase;
import database.models.DynamoDocument;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import security.AESEncryption;

import java.util.Calendar;
import java.util.Date;

/**
 * The model assumes a trusted authentication server (AS) and performs encryption based on three keys.
 * The server stores two sets of keys. The first set are derived from the clients passwords and are used to encrypt
 * communication between the AS and its clients.
 * The second set are the keys associated with servers that clients wish to communicate with.
 */
@Service
@Log4j2
public class TripleKeyAuthenticationService implements AuthenticationService {

    @Autowired
    Database<User> userDAO;

    @Autowired
    Database<ServerKey> serverDAO;

    /**
     * Verify if the user has permissions
     * @param username the username of the user to verify
     * @param passwordEncrypted the password of the user encrypted
     * @return the password decrypted
     */
    @Override
    public String verifyUser(final String username, final String passwordEncrypted) {
        final User user = this.userDAO.get(username);

        if (user == null) {
            log.warn("Unauthorized attempt.");
            throw new UnauthorizedException("The username specified does not exist.");
        }

        final String decrypt = AESEncryption.decryptAsString(user.getPassword(), passwordEncrypted);

        if (!user.getPassword().equals(decrypt)) {
            log.warn("Unauthorized attempt.");
            throw new UnauthorizedException("The username specified does not exist.");
        }

        return user.getPassword();
    }

    /**
     * Create the token that will be sent to the client
     * @param server the server that the client was to connect to
     * @param password the password of the client
     * @return
     */
    @Override
    public String createToken(final String server, final String password) {
        final String sessionKey = AESEncryption.generateKey();
        final Ticket ticket = new Ticket(sessionKey);
        final ServerKey serverKey = this.serverDAO.get(server);
        final String ticketEncrypted = AESEncryption.encrypt(serverKey.getKey(), ticket);
        final Token token = new Token(sessionKey, ticketEncrypted, server, this.getExpirationDate());
        return AESEncryption.encrypt(password, token);
    }

    /**
     * The expiration date when the token is not longer valid.
     * @return the date fo expiration
     */
    private Date getExpirationDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.HOUR_OF_DAY, 1);
        return calendar.getTime();
    }
}
