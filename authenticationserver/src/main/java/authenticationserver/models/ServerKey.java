package authenticationserver.models;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import database.annotations.TableName;
import database.models.DynamoDocument;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@DynamoDBTable(tableName="ServerKey")
public class ServerKey implements DynamoDocument {

    private String server;

    private String key;

    public ServerKey() {}

    public ServerKey(String server, String key) {
        this.server = server;
        this.key = key;
    }

    @DynamoDBHashKey(attributeName="server")
    public String getServer() {
        return server;
    }
    public void setServer(String server) {
        this.server = server;
    }

    @DynamoDBAttribute(attributeName = "key")
    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public String getPrimaryKey() {
        return this.server;
    }
}
