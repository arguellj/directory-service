package authenticationserver.models.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.sql.Date;
import java.sql.Timestamp;

@AllArgsConstructor
public class AuthenticationResponse {

    /**
     * Token encrypted with a key derived from the user's password passed to the AS.
     */
    @Getter
    private String token;
}
