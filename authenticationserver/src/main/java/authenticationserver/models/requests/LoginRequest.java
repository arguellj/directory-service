package authenticationserver.models.requests;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
public class LoginRequest {

    @Getter
    @NotNull
    private String message;

    @Getter
    @NotNull
    private String username;

    @Getter
    @NotNull
    private String server;
}
