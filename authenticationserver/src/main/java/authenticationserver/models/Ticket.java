package authenticationserver.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class Ticket {

    @Getter
    private String sessionKey;
}
