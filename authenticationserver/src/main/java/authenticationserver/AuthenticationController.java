package authenticationserver;

import authenticationserver.models.requests.LoginRequest;
import authenticationserver.models.responses.AuthenticationResponse;
import authenticationserver.service.AuthenticationService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
@Log4j2
public class AuthenticationController {

    @Autowired
    AuthenticationService service;

    @RequestMapping(method = POST, value = "/authenticate")
    public ResponseEntity<AuthenticationResponse> authenticate(@RequestBody @Valid LoginRequest request) {
        log.info(String.format("Verifying user %s", request.getUsername()));
        final String password = this.service.verifyUser(request.getUsername(), request.getMessage());
        log.info(String.format("User %s authenticated successfully against server %s",
                request.getUsername(), request.getServer()));
        final String token = this.service.createToken(request.getServer(), password);
        AuthenticationResponse response = new AuthenticationResponse(token);
        return ResponseEntity.ok(response);
    }
}
