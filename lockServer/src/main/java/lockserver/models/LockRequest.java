package lockserver.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class LockRequest {

    @Getter
    private boolean writeOperation;

    @Getter
    private String filePath;
}
