package lockserver;

import common.SecureCommunicator;
import common.models.ServerKey;
import common.models.Ticket;
import common.models.requests.SecureRequest;
import common.models.responses.SecureResponse;
import database.Database;
import lockserver.models.LockRequest;
import lockserver.services.LockService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
@Log4j2
public class LockServerController {

    @Value("${app.baseURL}")
    String baseUrl;

    @Autowired
    LockService lockService;

    @Autowired
    Database<ServerKey> serverKeyDAO;

    @RequestMapping(method = POST, value = "/lock")
    public ResponseEntity<SecureResponse> lock(@RequestBody @Valid SecureRequest request) {
        log.info("Locking file...");
        final Ticket ticket = SecureCommunicator.decryptTicket(request.getTicket(), this.baseUrl, this.serverKeyDAO);
        final LockRequest lockRequest = (LockRequest) SecureCommunicator.decryptMessageAsObject(
                ticket.getSessionKey(), request.getMessage(), LockRequest.class);

        this.lockService.lock(lockRequest.getFilePath(), lockRequest.isWriteOperation());

        return SecureCommunicator.getSecureResponse(ticket, "");
    }

    @RequestMapping(method = POST, value = "/release")
    public ResponseEntity<SecureResponse> release(@RequestBody @Valid SecureRequest request) {
        log.info("Releasing file...");
        final Ticket ticket = SecureCommunicator.decryptTicket(request.getTicket(), this.baseUrl, this.serverKeyDAO);
        final LockRequest lockRequest = (LockRequest) SecureCommunicator.decryptMessageAsObject(
                ticket.getSessionKey(), request.getMessage(), LockRequest.class);

        this.lockService.release(lockRequest.getFilePath(), lockRequest.isWriteOperation());

        return SecureCommunicator.getSecureResponse(ticket, "");
    }
}
