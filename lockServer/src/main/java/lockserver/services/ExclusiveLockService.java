package lockserver.services;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;

/**
 * Provides exclusive access to files by implementing write locking
 */
@Service
@Log4j2
public class ExclusiveLockService implements LockService {

    /**
     * Current clients using the lock.
     */
    private ConcurrentHashMap<String, Semaphore> locks = new ConcurrentHashMap<>();

    /**
     * Lock a file
     * @param filePath the file to lock
     * @param isWrite whether or not the operation is writing
     */
    @Override
    public void lock(String filePath, boolean isWrite) {
        if (!isWrite) {
            // only lock when write a file
            return;
        }

        this.locks.putIfAbsent(filePath, new Semaphore(1, true));

        try {
            this.locks.get(filePath).acquire();
        } catch (InterruptedException e) {
            log.error(e);
        }
    }

    /**
     * Releases the lock
     * @param filePath the file to release
     * @param isWrite whether or not the operation is writing
     */
    @Override
    public void release(String filePath, boolean isWrite) {
        if (!isWrite) {
            // only lock when write a file
            return;
        }

        if (!this.locks.containsKey(filePath)) {
            return;
        }

        this.locks.get(filePath).release();

        // TODO: remove thread-safety the entry to save memory when there is no more threads waiting.
    }
}
