package lockserver.services;

public interface LockService {
    void lock(String filePath, boolean isWrite);

    void release(String filePath, boolean isWrite);
}
